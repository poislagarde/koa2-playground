'use strict';

function timeout(ms) {
    return new Promise(resolve =>
        setTimeout(resolve, ms)
    );
}

async function test() {
    console.log('start');
    await timeout(1000);
    console.log('stop');
}

test();

console.log('hello world');
