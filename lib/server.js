'use strict';

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

let test = (() => {
    var ref = (0, _asyncToGenerator3.default)(function* () {
        let a = new Date();
        console.log('start');
        yield timeout(1000);
        console.log('stop');
    });
    return function test() {
        return ref.apply(this, arguments);
    };
})();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function timeout(ms) {
    return new _promise2.default(resolve => setTimeout(resolve, ms));
}

test();

console.log('hello world');
//# sourceMappingURL=server.js.map